# Svelte Template with TS and Tailwind

Opinionated template to quickstart svelte projects.


## What includes?
* Typescript
* TailwindCSS
* Vite
* Eslint

## Getting started
1. Fork this repository
1. Install the dependencies. `npm install`
1. Start the project. `npm run dev`
1. To build use `npm run build`. Build to the `./dist` folder.

## Template Structure
The following structure is used in this template
```
├── dist
│   └── images/audios/fonts
├── src
│   ├── assets
│   │   └── images/audios/fonts
│   ├── utils
│   │   ├── apis.ts
│   │   ├── stores.ts
│   │   └── index.ts
│   ├── components
│   │   ├── Button.svelte
│   │   ├── Tile.svelte
│   │   └── Chart.svelte
│   ├── views
│   │   ├── Home.svelte
│   │   ├── About.svelte
│   │   └── Settings.svelte
│   ├── App.svelte
│   ├── global.css
│   ├── main.ts
│   └── vite-env.d.ts
├── config files
├── package.json
└── README.md
```

## Stores
The `svelte/store` module is used to handle data across the app. This template have 2 stores implemented
* State store: used to handle the reactive states of the app, in this template is used to handle the views.
* Data store: reactive data that is also saved on LocalStorage. Useful to save settings, tokens, etc.

More info on how to use in https://svelte.dev/docs#run-time-svelte-store