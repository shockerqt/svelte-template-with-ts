/// <reference types="svelte" />
/// <reference types="vite/client" />

type Route = 'home' | 'examples';

interface View {
  current: Route;
  params?: {
    [key: string]: any;
  }
}

interface State {
  view: View;
  [key: string]: any;
}

interface Data {
  version: string;
  homeText: string;
  [key: string]: any;
}
