import { writable } from 'svelte/store';

const saveDelay = 2000;

let timeout: number;

/**
 * Used to store volatile data.
 * Data will be deleted after closing the window.
 * e.g. Save the current view
 */
const createState = () => {
  const defaultState: State = {
    view: {
      current: 'home',
    },
  };

  const { set, subscribe, update } = writable<State>(defaultState);

  return {
    set,
    subscribe,
    update,
    // Custom functions
    switchView: (to: Route, params?: { [key: string]: string; }) =>
      update((value) => ({
        ...value,
        view: {
          current: to,
          params: params || undefined,
        },
      })),
  };
};

/**
 * Used to store persistent data on LocalStorage.
 * Write to storage after certain interval to not overload it.
 */
const createData = () => {
  const storedData = localStorage.getItem('data');
  const data = storedData && JSON.parse(storedData);
  const defaultData = {};

  const { subscribe, set, update } = writable<Data>(data || defaultData);

  subscribe((data) => {
    if (timeout) clearTimeout(timeout);

    timeout = setTimeout(() => {
      localStorage.setItem('data', JSON.stringify(data));
    }, saveDelay);
  });

  return {
    set,
    subscribe,
    update,
  };
};

export const state = createState();
export const data = createData();
