// Api calls

export const anApiCall = async () => {
  const response = await fetch("http://example.com/movies.json");
  const jsonData = await response.json();
  return jsonData;
};
